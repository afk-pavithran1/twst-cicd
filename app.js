const app = require('express')()

app.set('PORT', process.env.PORT || 5000)


app.get('/', (req, res) => {
    res.json('It Works')
})

app.listen(app.get('PORT'), ()=> console.log('server running'))